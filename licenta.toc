\contentsline {section}{\numberline {1}Introducere}{3}
\contentsline {section}{\numberline {2}Metodologii \IeC {\c s}i Tehnologii folosite \IeC {\^\i }n Testare Automat\IeC {\u a}}{4}
\contentsline {subsection}{\numberline {2.1}Conceptul ciclului de testare}{4}
\contentsline {subsection}{\numberline {2.2}Metodologii}{6}
\contentsline {subsubsection}{\numberline {2.2.1}TDD (Test Driven Development)}{6}
\contentsline {subsubsection}{\numberline {2.2.2}BDD (Behaviour Driven Development)}{8}
\contentsline {subsection}{\numberline {2.3}Tehnologii}{10}
\contentsline {subsubsection}{\numberline {2.3.1}Selenium WebDriver}{10}
\contentsline {subsubsection}{\numberline {2.3.2}RestAssured}{11}
\contentsline {subsubsection}{\numberline {2.3.3}Cucumber JVM}{12}
\contentsline {subsubsection}{\numberline {2.3.4}JUnit}{13}
\contentsline {subsubsection}{\numberline {2.3.5}AssertJ}{13}
\contentsline {section}{\numberline {3}Prezentarea Aplica\IeC {\c t}iei Practice \IeC {\^\i }n Testare}{14}
\contentsline {subsection}{\numberline {3.1}Interfa\IeC {{\textcommabelow t}}\IeC {\u a}}{14}
\contentsline {subsubsection}{\numberline {3.1.1}Autentificarea \IeC {\c s}i \IeC {\^\i }nregistrarea utilizatorilor}{14}
\contentsline {subsubsection}{\numberline {3.1.2}Profilul utilizatorului}{16}
\contentsline {paragraph}{\numberline {3.1.2.1}Post\IeC {\u a}ri de mesaje}{17}
\contentsline {paragraph}{\numberline {3.1.2.2}Urm\IeC {\u a}rirea altor utilizatori}{18}
\contentsline {subsubsection}{\numberline {3.1.3}C\IeC {\u a}utare utilizatori \IeC {\c s}i post\IeC {\u a}ri}{18}
\contentsline {subsubsection}{\numberline {3.1.4}Bara de navigare}{19}
\contentsline {subsection}{\numberline {3.2}Server}{20}
\contentsline {section}{\numberline {4}Analiza Aplica\IeC {\c t}iei \IeC {\^\i }n Contextul Test\IeC {\u a}rii}{24}
\contentsline {subsection}{\numberline {4.1}Arhitectura aplica\IeC {{\textcommabelow t}}iei web}{24}
\contentsline {subsubsection}{\numberline {4.1.1}Baza de Date}{24}
\contentsline {subsubsection}{\numberline {4.1.2}Servicii}{27}
\contentsline {subsection}{\numberline {4.2}Arhitectura libr\IeC {\u a}riei de testare}{29}
\contentsline {subsection}{\numberline {4.3}Teste specifice pentru verificarea interfe\IeC {\c t}ei web}{29}
\contentsline {subsection}{\numberline {4.4}Teste specifice pentru verificarea serverului}{29}
\contentsline {subsection}{\numberline {4.5}Rapoarte create pe baza execu\IeC {\c t}iei testelor}{29}
\contentsline {section}{\numberline {5}Concluzie privind Stadiul Actual al Industriei}{30}
\contentsline {section}{Bibliografie}{31}
